<?php

namespace App\Services;


use App\Repositories\CallRepository;
use Illuminate\Support\Facades\DB;

class CallServer extends BaseService
{

    /**
     * @var CallRepository
     */
    protected $callRepository;


    public function __construct(CallRepository $callRepository)
    {
        $this->callRepository = $callRepository;

    }

    public function free($id)
    {
        try {
            DB::beginTransaction();
            $user = $this->callRepository->freeCall($id);
            DB::commit();
            return $this->success([
                'data' => $user
            ], 'success');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->error('error_free_data');
        }
    }

}
