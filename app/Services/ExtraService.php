<?php

namespace App\Services;


class ExtraService extends BaseService
{
    public function childEither($data)
    {
        $n = log(((int)$data['n'] ^ 3));
        return $this->success([
            'data' => $n,
        ], 'success');
    }

    public function givenSum($data)
    {
        $array = $data['a'];
        $sum = $data['key'];
        sort($array);
        $arr_size = sizeof($array) - 1;
        $lower = 0;
        while ($lower < $arr_size) {
            if ($array[$lower] + $array[$arr_size] == $sum)
                return $this->success([
                    'result' => $array[$lower] .'-'. $array[$arr_size],
                ], 'yes');
            else if ($array[$lower] + $array[$arr_size] < $sum)
                $lower++;
            else
                $arr_size--;
        }
        return $this->error('no', [
            'result' => '',
        ]);
    }

}
