<?php

namespace App\Services;


use App\Repositories\AuthRepository;
use App\Serializers\UserShowCollection;
use App\Serializers\UserShowSerializer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class AuthService extends BaseService
{
    /**
     * @var AuthRepository
     */
    protected $authRepository;


    public function __construct(AuthRepository $authRepository)
    {
        $this->authRepository = $authRepository;

    }

    public function registerUser($data)
    {
        try {
            DB::beginTransaction();
            $user = $this->authRepository->createUser($data);
            DB::commit();
            $accessToken = $user->createToken('authToken')->accessToken;
            return $this->success([
                'access_token' => $accessToken,
                'user' => $user
            ], 'success create');

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->error('error_create_data');
        }

    }

    public function loginUser($data)
    {
        if (!auth()->attempt($data)) {
            return response(['message' => 'Invalid credentials']);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;
        return $this->success([
            'access_token' => $accessToken,
            'user' => auth()->user()
        ], 'success login');

    }


}
