<?php

namespace App\Jobs;

use App\StrategyPattern\StrategyDirector;
use App\StrategyPattern\StrategyManager;
use App\StrategyPattern\StrategyRespondent;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CallCenter implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $call;
    private $_class_respondent;
    private $_class_manager;
    private $_class_director;
    private $status = 0;
    private $strategy;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($call)
    {
        $this->call = $call;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        switch ($this->status) {
            case $this->status != 1:
                $this->strategy = new StrategyRespondent();
                $this->status = 1;
                break;
            case $this->status != 2:
                $this->strategy = new StrategyManager();
                $this->status = 2;
                break;
            case $this->status != 3:
                $this->strategy = new StrategyDirector();
                $this->status = 3;
                break;
        }
    }
}
