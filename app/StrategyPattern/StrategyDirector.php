<?php
namespace App\StrategyPattern;


use App\Repositories\CallRepository;
use Illuminate\Support\Facades\DB;

class StrategyDirector implements StrategyInterface {

    /**
     * @var CallRepository
     */
    protected $callRepository;


    public function __construct(CallRepository $callRepository)
    {
        $this->callRepository = $callRepository;

    }


    public function CallCenter($call) {
        try {
            DB::beginTransaction();
            $call = $this->callRepository->sendToDataBase($call);
            DB::commit();
            return $this->success([
                'data' => $call
            ], 'success');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->error('error_free_data');
        }

    }
}

