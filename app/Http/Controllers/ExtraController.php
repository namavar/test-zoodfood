<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChildRequest;
use App\Http\Requests\SumRequest;
use App\Services\AuthService;
use App\Services\ExtraService;
use Illuminate\Http\Request;

class ExtraController extends Controller
{
    protected $extraService;

    /**
     * CompaniesController constructor.
     *
     * @param AuthService $authService
     */
    public function __construct(ExtraService $extraService)
    {
        $this->extraService = $extraService;
    }

    public function child(ChildRequest $request){
        $response = $this->extraService->childEither($request->all());
        return response()->json($response);
    }

    public function givenSum(SumRequest $request){
        $response = $this->extraService->givenSum($request->all());
        return response()->json($response);
    }
}
