<?php

namespace App\Http\Controllers;

use App\Http\Requests\FreeRequest;
use App\Jobs\CallCenter;
use App\Services\AuthService;
use App\Services\CallServer;
use App\Services\ExtraService;
use Illuminate\Http\Request;

class CallController extends Controller
{

    protected $callService;

    /**
     * CompaniesController constructor.
     *
     * @param AuthService $authService
     */
    public function __construct(CallServer $callService)
    {
        $this->callService = $callService;
    }

    public function call(Request $request){

        $this->dispatch(new CallCenter($request->all()));
    }

    public function freeCall(FreeRequest $request){
        $response = $this->callService->free($request->all('id'));
        return response()->json($response);
    }
}
