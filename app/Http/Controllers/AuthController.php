<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Services\AuthService;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    /**
     * @var AuthRepository
     */
    protected $authService;

    /**
     * CompaniesController constructor.
     *
     * @param AuthService $authService
     */
    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function register(RegisterRequest $request){

        $response = $this->authService->registerUser($request->all());
        return response()->json($response);
    }

    public function login(LoginRequest $request){
        $response = $this->authService->loginUser($request->all());
        return response()->json($response);
    }
}
