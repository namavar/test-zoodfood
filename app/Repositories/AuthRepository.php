<?php

namespace App\Repositories;


use App\User;

/**
 * Class UserRepository.
 *
 * @package namespace App\Repositories;
 */
class AuthRepository
{
    private $model;

    public function __construct(User $User)
    {
        $this->model = $User;
    }

    public function createUser($data)
    {
        return $this->model->create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

}
