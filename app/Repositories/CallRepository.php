<?php

namespace App\Repositories;


use App\Entities\Call;

/**
 * Class CallRepository.
 *
 * @package namespace App\Repositories;
 */
class CallRepository
{
    private $model;

    public function __construct(Call $model)
    {
        $this->model = $model;
    }

    public function sendToDataBase($data)
    {
        return $this->model->create([
            'call' => $data['call'],
            'status' => 1,
        ]);
    }

    public function freeCall($id){
        return $this->model->where('id', $id)->update([
            'status' => 0
        ]);
    }

}
