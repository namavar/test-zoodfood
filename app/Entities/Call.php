<?php

namespace App\Entities;

use App\Entities\Scopes\TrashScope;
use Illuminate\Database\Eloquent\Model;

class Call extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'call',
        'status'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new TrashScope());
    }
}
