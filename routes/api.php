<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::prefix('v1')->group(function(){
    Route::Post('/register', 'AuthController@register');
    Route::Post('/login', 'AuthController@login');

    Route::group(['middleware' => 'auth:api'], function(){


    });
});
Route::Post('/call', 'CallController@call');
Route::Post('/free-call', 'CallController@freeCall');
Route::Post('/child', 'ExtraController@child');
Route::Post('/givenSum', 'ExtraController@givenSum');

